
# graphviz step by step tutorial

## 0. most basic graph

```dot
graph {
    sparks -- fire
}
```

## 1. most basic digraph

```dot
digraph {
    sparks -> fire
    
    //  fire -> sparks
}
```

## 2. labels for node and edges

```dot
digraph {
    
    sparks [label="⚡"]
    fire   [label="🔥"]
    
    sparks -> fire
    
    // sparks -> fire [label="causes"]
}
```

## 3. the poem from the doc with xlabel


```dot
digraph {
    
    sparks [label="⚡" xlabel="sparks"]
    fire   [label="🔥" xlabel="fire"]
     
    sparks -> fire [label="causes" xlabel="sometimes"]
}
```

## 4. other attributes

<https://graphviz.org/docs/nodes/>
<https://graphviz.org/docs/edges/>

### 4.A. linebreaks & spacing labels (prettier)

```dot
digraph {
    
    sparks [label="⚡" xlabel="sparks"]
    fire   [label="🔥" xlabel="fire"]
     
    sparks -> fire [label="  causes" xlabel="sometimes  "]
    
    paper[label="\n something \n very very\n important? \n\n"]
    
    fire -> paper [label="  burn" xlabel="& could  "]
}
```

### 4.B. subgraph with rank constraint

making a graph inside of my digraph, and actually every node in this subgrand have to have the same rank

```dot
digraph {
    
    {  rank=same;
       spark_1  [label="⚡"]
       spark_2  [label="⚡"  xlabel="sparkles"]
       spark_3  [label="⚡"]
    }

    fire [label="🔥" xlabel="fire"]
     
    spark_1 -> fire [label="  sometimes "]
    spark_2 -> fire [label="  causes "]
    spark_3 -> fire
    
    paper[label="\n something \n very very\n important? \n\n"]
    
    fire -> paper [label="  burn" xlabel="& could  "]
}
```

### 4.C. node & edge styling

using `shape` and `style` to give it more personnality

```dot
digraph {
    
    {  rank=same;
       spark_1  [label="⚡"  shape=plaintext]
       spark_2  [label="⚡"  xlabel="sparkles" shape=plaintext]
       spark_3  [label="⚡"  shape=plaintext]
    }

    fire [label="🔥" xlabel="fire"]
     
    spark_1 -> fire [style=dashed label="  sometimes "]
    spark_2 -> fire [style=dashed label="  causes "]
    spark_3 -> fire [style=dashed]
    
    paper[label="\n something \n very very\n important? \n\n" shape=note]
    
    fire -> paper [label="  burn" xlabel="& could  "]
}
```

### 4.D. global node & edge styling

plus changing direction: at this point you can realize that nodes go from top to bottom, what if we want to make them go from left to right?

```dot
digraph {
    
    //rankdir=LR

    node [fontname="Times-bold" shape=plaintext]
    edge [fontname="Times-italic" style=dashed]
    
    {  rank=same;
       spark_1  [label="⚡"]
       spark_2  [label="⚡" xlabel="sparkles"]
       spark_3  [label="⚡"]
    }

    fire [label="🔥" xlabel="fire" shape=ellipsis]
     
    spark_1 -> fire [label="  sometimes"]
    spark_2 -> fire [label="  causes"]
    spark_3 -> fire
    
    paper[label="\n something \n very very\n important? \n\n" shape=note]
    
    fire -> paper [label="  burn" xlabel="& could  " style="solid"]
}
```
     
     