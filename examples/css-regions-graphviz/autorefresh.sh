#!/bin/bash

input=$1;
filename=$(basename $input);

while true
do
	#wget http://pads.osp.kitchen/p/css-regions.graphviz/export/txt -O $filename.dot
	dot -Tpdf $filename.dot > $filename.pdf
	dot -Tsvg $filename.dot > $filename.svg
	sleep 5
done
