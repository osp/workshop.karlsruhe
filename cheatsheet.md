## Shape

It is possible to set a different shape for nodes
You can set it for all nodes with:
`node [shape=house];`

Or. For an individual node with:
`"A star!" [shape=star];`

<https://www.graphviz.org/doc/info/shapes.html>


## Difference in name and label

For nodes with complex labels, you can make an alias by giving the node a name, and giving it a label:
`"Node with complex text";` becomes `complexnode [label="Node with complex text"]`;

<https://www.graphviz.org/docs/attrs/label/>


## HTML in a label

It is possible to insert a subset of HTML in the label.

`nodename [label=<part of this label is <b>bold?</b>>]`

- `<b>`
- `<br/>`
- `<font>`
- `<hr>`
- `<i>`
- `<img> (in <td>…</td> only)`
- `<o>`
- `<s>`
- `<sub>`
- `<sup>`
- `<table>`
- `<tr>`
- `<u>`
- `<vr> `

## Make the label clickable

With the URL attribute a label becomes a link!
`"Our website!" [URL="http://osp.kitchen"]`

Links only work in PDF and svg output.
<https://www.graphviz.org/docs/attrs/URL/>


## Different splines

Adjusts the shape of the edges connecting the nodes.

- `graph [spline=line]`
- `graph [spline=polyline]`
- `graph [spline=spline]`

## Separting nodes

Adjusts the spacing between nodes for the `dot` engine.

`node [ranksep=2.5, nodesep=0.8];` 

<https://www.graphviz.org/docs/attrs/nodesep/>
<https://www.graphviz.org/docs/attrs/ranksep/>


## Changing the font of the node

mynode [fontname="AVHershey Simplex"]

<https://www.graphviz.org/docs/attrs/fontname/>


## Generating a bigger image

With the `dpi` attribute the resolution of the graph can be controlled:
`graph [dpi=300]`

<https://www.graphviz.org/docs/attrs/dpi/>