from flask import Flask, request, render_template

import requests
import graphviz
from markdown import markdown

import pydot
import textwrap

import re

app = Flask(__name__)

title = 'etherdot'
etherpads = 'http://pads.osp.kitchen/p/'
prefix = 'etherdot'

engines = ['dot', 'neato', 'fdp', 'sfdp', 'circo', 'twopi', 'osage', 'patchwork']


@app.route("/")
def index():

    pad = etherpads + prefix

    return render_template(
        'index.html',
        title = title,
        pad = pad)

@app.route("/index--md")
def index_md():

    pad = etherpads + prefix
     
     # get pad content
    print('  getting ' + pad)
    pad_export = requests.get(pad + '/export/txt')

    # markdown parse
    content = markdown(pad_export.text)

    return render_template(
        'md.html',
        content = content)

@app.route("/<id>")
def interface(id):

    pad = etherpads + prefix + '-' + id

    # render templates
    return render_template(
        'page.html',
        id = id,
        pad = pad,
        title = title,
        engines = engines)


# only the svg graph that constently regenerate itself
# so it can actualize automatically in the left window
@app.route("/<id>--svg")
def graph(id):

    print('• ' + id )

    pad = etherpads + prefix + '-' + id

    # GET parameter
    get_engine = request.args.get('engine')
    print('  engine ' + get_engine)

    # get pad content
    print('  getting ' + pad)
    pad_export = requests.get(pad + '/export/txt')
    graphs_string = pad_export.text

    # text-wrapping
    # graphs = pydot.graph_from_dot_data(str(pad_export.text))
    # for graph in graphs:
    #     for node in graph.get_nodes():
    #         node.set_label("helloooo")
    #         # if node.get_label():
    #             # print(node)
    #             # node.set_label( textwrap.fill(node.get_label(), max_char, break_long_words=False) )
    # graphs_string = "".join([graph.to_string() for graph in graphs])

    # text-wrapping with regex
    get_maxchar = request.args.get('maxchar')
    if get_maxchar:
        print('  maxchar ' + get_maxchar)
        def wrap_label(m):
            # cleaned_label = m.group(1).replace("\\n", "")
            wrapped_label = textwrap.fill(m.group(1), int(get_maxchar), break_long_words=False)
            return "label=\"{l}\"".format(l=wrapped_label)
        graphs_string = re.sub(r"label *?= *?\"(.+)\"", wrap_label, graphs_string)
    
    # generate graph
    print('  generating')
    dot_encode =  bytes(graphs_string, 'utf-8')
    graph_svg = graphviz.pipe(engine = get_engine, data = dot_encode, format="svg")
    svg_decode = graph_svg.decode('utf-8')

    # render templates
    return render_template(
        'graph.html',
        graph = svg_decode)


if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0')