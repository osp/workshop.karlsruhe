

let graph_frame = document.getElementById('graph__frame');

// buttons
let dot_button = document.getElementById('dot-button');
let print_button = document.getElementById('print-button');

// selects
let engine_select = document.getElementById('engine-select');

// label wrap
let labeltw_checkbox = document.getElementById('labeltw-checkbox');
let maxchar_input = document.getElementById('maxchar-input');

//  BUTTONS
//  ----------------------------------------------------------------

dot_button.addEventListener('click', function(){
    // refresh
    graph_frame.contentWindow.location.reload();
});

print_button.addEventListener('click', function(){
    // print iframe content
    graph_frame.contentWindow.print();
});

//  GET-PARAM
//  ----------------------------------------------------------------

function updateGET(param, value){

    // object from GET parameters
    let [base_src, params_src] = graph_frame.src.split("?");
    let params = new URLSearchParams(params_src);

    // update param
    params.set(param, value);

    // reconstituate URL
    let new_src = base_src + "?" + params.toString();
    console.log(new_src);

    // set and refresh
    graph_frame.src = new_src;
}

//  SELECTS
//  ----------------------------------------------------------------

engine_select.addEventListener('input', function(){
    updateGET('engine', engine_select.value);
});

updateGET('engine', engine_select.value);


//  MAXCHAR CHECK&INPUT
//  ----------------------------------------------------------------

labeltw_checkbox.addEventListener('input', function(){
    if (labeltw_checkbox.checked){
        updateGET('maxchar', maxchar_input.value);
    } else{
        updateGET('maxchar', '');
    }
});

maxchar_input.addEventListener('input', function(){
    if (labeltw_checkbox.checked){
        updateGET('maxchar', maxchar_input.value);
    }
});
if (labeltw_checkbox.checked){
    updateGET('maxchar', maxchar_input.value);
}