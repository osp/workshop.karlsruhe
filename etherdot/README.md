# ether--dot

a tool to collectively write **DOT language** on **etherpads**.

```dot
digraph{
    DOT      -> EtherPad   [label="write"]
    EtherPad -> GraphViz   [label="generate"]
    GraphViz -> DOT        [label="reflect"]
}
```

it can also serves to archive/index/browse/document/tell graphs made by others through an **index page/pad** in markdown.

it includes special features such as

* **CSS styling**, 
* **multi-seed generation**, 
* **svg hyperlink export**, 
* **label text-wrapping**
* and printing pdf through the browser with each graph on a new page.

initiated for a Graphviz workshop in the University of design of Karlsruhe that took place during the 22nd of June 2023.

## use

install requierement

    pip3 install requierement.txt


launch

    python3 app.py


you can create a new graph by appending some words to the url like

    http://localhost:5000/mygraph


## setup documentation

This setup uses a [Flask application](https://flask.palletsprojects.com/en/2.2.x/), in order to dynamically instance new graphs.

Each graph is associated to a prefixed pad on a specific [Etherpad]() instance.

It uses [Graphviz python module]() to parse the content of the pad in DOT language and render an `svg`, that is then included back in the page, on the press of the DOT button.

In order to use the content of pads as stylesheet, an ajax request is requiered. It fills the `<style>` tags through a `data-pad` attribute, because some browser (?) show a mimetype restriction error (expecting `text/css` instead of `text/plain`).

## todo

* css styling
* multi seed
* svg hyperlink export